#! bash

[ -z "$3" ] && echo "EXIT: argument missing" && exit 1

trap "exit 0" 2 # CTRL+C

case $(adb get-state) in
  device*)
    echo "pull: building file list..." ;;
  *)
    echo "EXIT: adb offline"
    exit 1 ;;
esac

case $1 in
  pull)
    # convert path dos2cygwin
    src="${2/%\//}"
    dir="${3//\\//}"
    dir="${dir/%\//}"
    if [ "${dir:1:1}" = ':' ]
      then
        dir="${dir,}"
        drive="${dir:0:2}"
        dir="${dir/#$drive//cygdrive/${drive:0:1}}"
    fi

    # check target dir
    [ -d "$dir" ] || exit 0

    # pull target dir list from source
    adb shell "cd '${src%/*}'; find -H '${src##*/}' -type d -exec tar --no-recursion -hcf /data/local/tmp/target.list.tar {} + 2> /dev/null"
    adb shell "mkdir -p /data/local/tmp/winadb-sync; tar -xf /data/local/tmp/target.list.tar -C /data/local/tmp/winadb-sync 2> /dev/null"
    adb pull -a "/data/local/tmp/winadb-sync/${src##*/}" "${3/%\\/}" > /dev/null
    adb shell find /data/local/tmp/winadb-sync -delete > /dev/null
    adb shell rm /data/local/tmp/target.list.tar

    # get file list from source
    adb shell "find -H '$src' -type f -print0 > /data/local/tmp/source.list 2> /dev/null"
    adb pull /data/local/tmp/source.list > /dev/null
    adb shell rm /data/local/tmp/source.list

    # pull files	
    while IFS= read -r -d '' file
      do
        # convert path cygwin2dos
        target="$dir/${file#*${src%/*}/}"
        if [ "${target:1:8}" = 'cygdrive' ]
          then
            win="${target#/cygdrive/?/}"
            win="${drive^}\\${win////\\}"
          else
            win="${target////\\}"
        fi
        if ! [ -f "$target" ]
          then
            adb pull -a "$file" "$win" || { > "$target"; echo -e "${win//\\/\\\\}"\\r >> sync-errors.txt; }
        fi
    done < source.list
    echo
    echo "pull: sync done"
    exit 0 ;;

  push)
    echo "adb push --sync $2 $3"
    adb push --sync "$2" "$3"
    exit 0 ;;

  *)
    echo "ERROR: invalid command"
    exit 1 ;;
esac
